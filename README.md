# Kidsloop Online Test

# Overview

<!--break-->

# FTP Connection via FileZilla

This document describes how to use SFTP (via the open source FileZilla application) to transfer data between your computer.
<!--break-->

1. If you don't already have FileZilla, download it from the [FileZilla website](https://filezilla-project.org) and follow the installation instructions. (Versions of FileZilla are available for Windows, Mac OS X, and Linux.)
2. Open FileZilla.
3. From the "File" menu, select "Site Manager..."
4. In Site Manager:
    - Click the New Site button.
    - In the Host field, enter: `61.28.235.230`
    - From the Protocol: menu, select: `FTP - File Transfer Protocol`
    - In the Logon Type: menu, select:  `Normal`
    - In the User: field, enter: (Your actual username)
    - In the Password: field, enter: (Your actual password)
    - Click the Connect button.
5. To upload files or folders from your computer to Savio:
    - Select one or more file(s) or folder(s) in FileZilla's "Local Site" pane.
    - Either drag those items into FileZilla's "Remote Site" pane, or right-click on a selected item, then select "Upload" from the contextual menu that appears.
    - Every time that the Enter Password dialog appears, complete _every_ sub-step under step 5, above.
6. To download files or folders from Savio to your computer:
    - Select one or more file(s) or folder(s) in FileZilla's "Remote Site" pane.
    - Either drag those items into FileZilla's "Local Site" pane, or right-click on a selected item, then select "Download" from the contextual menu that appears.
    - Every time that the Enter Password dialog appears, complete _every_ sub-step under step 5, above.

# Add a new FTP user to an existing container

Exec to container and run command
```bash
mkdir /home/vsftpd/myuser
echo -e "myuser\nmypass" >> /etc/vsftpd/virtual_users.txt
/usr/bin/db_load -T -t hash -f /etc/vsftpd/virtual_users.txt /etc/vsftpd/virtual_users.db
exit
```

<!--break-->